#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------------
# Created By  : Christopher Woell <christopher.woell@web.de>
# Created Date: 2022/04/03
# version ='0.9'
# ----------------------------------------------------------------------------------
""" Extractor for acquiring audio track titles from MP3 playlists in .m3u format """
# ----------------------------------------------------------------------------------
# Imports
# ----------------------------------------------------------------------------------
import platform
from tkinter import *
from tkinter import filedialog as fd

# Get platform-dependent path separator
if platform.system() == "Windows":
    path_sep = '\\'
elif platform.system() == "Linux" or platform.system() == "Darwin":
    path_sep = '/'
else:
    raise Exception("Not supported on this platform")

# Main window
root = Tk()
root.resizable(False, False)
root.title("Tapedeck-Liebhaber Title Extractor")
root.iconphoto(False, PhotoImage(file="pictures/extractor_icon.png"))

# Application logo
img1 = PhotoImage(file="pictures/extractor_logo.png")
extractor_logo = Label(root, image=img1)
extractor_logo.grid(column=1, row=0, rowspan=1, columnspan=2, ipadx=5, pady=5, sticky=N+E+S+W)

# Winamp logo
img2 = PhotoImage(file="pictures/winamp_logo.png")
winamp_logo = Label(root, image=img2)
winamp_logo.grid(column=2, row=1, rowspan=1, ipadx=5, pady=5, sticky=N+E+S+W)


def open_winamp_file():
    """
    Playlist file processing
    """
    filetypes = (("Playlist files", "*.m3u"), ("All files", "*.*"))
    file = fd.askopenfilename(title="Datei öffnen", initialdir='/', filetypes=filetypes)
    pathlabel.config(text=file)

    try:
        playlist = open(file, 'r')
        tracks = playlist.readlines()
        for track, num in zip(tracks, range(1, len(tracks) + 1)):
            track = f"{num:02}. " + track.rsplit(path_sep, 1)[1].split('.', 1)[0] + '\n'
            text.insert(END, track)
        playlist.close()

    except (FileNotFoundError, TypeError):
        pass


# Winamp file dialog
winamp_button = Button(root, text="Winamp-Playlist öffnen", command=open_winamp_file)
winamp_button.grid(column=1, row=1, rowspan=1, ipadx=5, pady=5)

# Textbox
text = Text(root, height=25)
text.grid(column=0, row=0, rowspan=25, ipadx=5, pady=5, sticky=N+E+S+W)

# Playlist file path
pathlabel = Label(root)
pathlabel.grid(column=0, row=26, rowspan=1, ipadx=5, pady=5, sticky=W)

# Run main window
root.mainloop()
